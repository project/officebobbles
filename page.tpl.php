<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?> 
  </head>
  <body>
<div class="topnav">
  <?php if ($topnav): ?>
  <?php print $topnav; ?>
  <?php endif; ?>
</div>
<div class="header">
  <?php if ($header): ?>
  <?php print $header; ?>
  <?php endif; ?>
</div>
<div id="content-container">
  <div class="left">
    <?php if ($left): ?>
    <?php print $left; ?>
    <?php endif; ?> 
  </div>

  <div class="middle">
    <?php if ($pre_content): ?>
    <div id="pre-content">
    <?php print $pre_content; ?>
    </div>
    <?php endif; ?> 
    <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
    <div id="breadcrumb" class="alone">
        <?php print $breadcrumb; ?>
    </div>   
    <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
    <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
    <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
    <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
    <?php if ($show_messages && $messages): print $messages; endif; ?>
    <?php print $help; ?>   
    <?php print $content ?>
    <?php if ($post_content): ?>
    <div id="post-content">
    <?php print $post_content; ?>
    </div>
    <?php endif; ?>
    <?php print $feed_icons ?>
  </div>
  <div class="right">
  <?php if ($right): ?>
  <?php print $right; ?>
  <?php endif; ?>  
  </div>
</div>
<div class="footer">
<?php if ($footer): ?>
<?php print $footer; ?>
<?php endif; ?>  
</div>
  <?php print $closure ?>
  </div>
  </body>
</html>
