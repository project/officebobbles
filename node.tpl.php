<?php
?>
<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
	<?php if ($page == 0): ?>
    	<div class="bg-h2">
			<?php print $picture ?>
            <h2 class="title"><a href="<?php print $node_url ?>"><?php print $title ?></a></h2>
            <span class="submitted"><?php print $submitted ?></span>
        </div>
    <?php endif; ?>
  
    <div class="content"><?php print $content ?></div>
    
    <?php if ($links): ?>
        <div class="links">&raquo; <?php print $links ?></div>
    <?php endif; ?>
    
    <?php if (!$teaser): ?>
    <?php if ($terms) { ?><div class="taxonomy"><span><?php print t('Tags') ?></span> <?php print $terms?></div><?php } ?>
    <?php endif; ?>
    
</div>